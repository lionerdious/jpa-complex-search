package com.example.searchdemo.api;

import com.example.searchdemo.domain.Person;
import com.example.searchdemo.service.PersonService;
import com.example.searchdemo.service.request.CreatePersonRequest;
import com.example.searchdemo.service.request.PersonFilterRequest;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;



@RestController
@RequestMapping("/person")
@AllArgsConstructor
@Slf4j
public class PersonRestController {
    private final PersonService personService;

    @PostMapping
    public ResponseEntity savePerson(@RequestBody CreatePersonRequest createPersonRequest){
        return ResponseEntity.ok(personService.savePerson(createPersonRequest));

    }

    @GetMapping
    public ResponseEntity getPerson(@PageableDefault Pageable pageable, PersonFilterRequest personFilterRequest){
        log.info("Get person page request : pageable={}, personFilterRequest=");
        Page<Person> personPage = personService.getPerson(pageable, personFilterRequest);
        return ResponseEntity.ok(new PageImpl<>(personPage.getContent(), personPage.getPageable(),
                personPage.getTotalElements()));
    }
}
