package com.example.searchdemo.service;

import com.example.searchdemo.domain.Person;


import com.example.searchdemo.repo.PersonRepository;
import com.example.searchdemo.service.request.PersonFilterRequest;
import com.example.searchdemo.service.request.CreatePersonRequest;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;

@Service
@AllArgsConstructor
public class PersonServiceImpl implements PersonService{

    private final PersonRepository personRepository;

    @Override
    public Page<Person> getPerson(Pageable pageable, PersonFilterRequest personFilterRequest) {
        Objects.requireNonNull(pageable, "pageable cannot be null");
        Specification<Person> accountSpecification = getSpecification(personFilterRequest);
        if (Objects.nonNull(accountSpecification)) {
            return personRepository.findAll(accountSpecification, pageable);
        }
        return personRepository.findAll(pageable);
    }

    private Specification<Person> getSpecification(PersonFilterRequest filterRequest) {
        if (Objects.isNull(filterRequest)) {
            return null;
        }
        PersonSpecificationBuilder builder = PersonSpecificationBuilder.createSpecificationBuilder();
        Optional.ofNullable(filterRequest.getFirstName()).ifPresent(firstName ->
                builder.with(PersonSpecification.SearchCriteriaKey.FIRST_NAME.getValue(), firstName));
        Optional.ofNullable(filterRequest.getLastName()).ifPresent(lastName ->
                builder.with(PersonSpecification.SearchCriteriaKey.LAST_NAME.getValue(), lastName));
        Optional.ofNullable(filterRequest.getNationalId()).ifPresent(nationalId ->
                builder.with(PersonSpecification.SearchCriteriaKey.NATIONAL_ID.getValue(), nationalId));
        Optional.ofNullable(filterRequest.getEmail()).ifPresent(email ->
                builder.with(PersonSpecification.SearchCriteriaKey.EMAIL.getValue(), email));
        return builder.build();
    }

    @Override
    public Person savePerson(CreatePersonRequest createPersonRequest) {
        Objects.requireNonNull(createPersonRequest, "Create person request cannot be null");
        Person person = new Person();
        person.setFirstName(createPersonRequest.getFirstName());
        person.setLastName(createPersonRequest.getLastName());
        person.setNationalId(createPersonRequest.getNationalId());
        person.setEmail(createPersonRequest.getEmail());
        return personRepository.save(person);
    }
}
