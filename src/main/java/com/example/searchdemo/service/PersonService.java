package com.example.searchdemo.service;

import com.example.searchdemo.domain.Person;
import com.example.searchdemo.service.request.PersonFilterRequest;
import com.example.searchdemo.service.request.CreatePersonRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface PersonService {
    Page<Person> getPerson(Pageable pageable, PersonFilterRequest personFilterRequest);
    Person savePerson(CreatePersonRequest createPersonRequest);

}

