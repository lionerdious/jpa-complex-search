package com.example.searchdemo.service;

import com.example.searchdemo.domain.Person;
import com.example.searchdemo.search.SearchCriteria;
import org.springframework.data.jpa.domain.Specification;

import java.util.ArrayList;
import java.util.List;

public class PersonSpecificationBuilder {
    private final List<SearchCriteria> params;
    private PersonSpecificationBuilder() {
        this.params = new ArrayList<>();
    }

    public static PersonSpecificationBuilder createSpecificationBuilder() {
        return new PersonSpecificationBuilder();
    }

    public PersonSpecificationBuilder with(String key, Object value) {
        params.add(SearchCriteria.createSearchCriteria(key, null, value));
        return this;
    }

    public Specification<Person> build() {
        if (params.isEmpty()) {
            return null;
        }

        List<Specification<Person>> specifications = new ArrayList<>();
        for (SearchCriteria searchCriteria : params) {
            specifications.add(PersonSpecification.of(searchCriteria));
        }

        Specification<Person> result = specifications.get(0);
        for (int i = 1; i < specifications.size(); i++) {
            result = Specification.where(result).and(specifications.get(i));
        }
        return result;
    }
}
