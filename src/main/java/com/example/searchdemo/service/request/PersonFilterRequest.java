package com.example.searchdemo.service.request;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.math.BigDecimal;

@NoArgsConstructor
@Data
@ToString
public class PersonFilterRequest {
    private String firstName;
    private String lastName;
    private String nationalId;
private String email;//TO be deleted
}
