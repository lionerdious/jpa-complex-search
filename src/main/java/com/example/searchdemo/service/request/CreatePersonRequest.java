package com.example.searchdemo.service.request;

import lombok.Data;

@Data
public class CreatePersonRequest {
    private String firstName;
    private String lastName;
    private String nationalId;
    private String email;
}
