package com.example.searchdemo.service;

import com.example.searchdemo.domain.Person;
import com.example.searchdemo.search.SearchCriteria;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Arrays;

 class PersonSpecification implements Specification<Person> {
    private final SearchCriteria criteria;
    private PersonSpecification(SearchCriteria criteria) {
        this.criteria = criteria;
    }

    public static PersonSpecification of(SearchCriteria criteria) {
        return new PersonSpecification(criteria);
    }

    @Override
    public Predicate toPredicate(Root<Person> root, CriteriaQuery<?> criteriaQuery,
                                 CriteriaBuilder criteriaBuilder) {
        SearchCriteriaKey key = SearchCriteriaKey.getKeyByValue(criteria.getKey());
        Object value = criteria.getValue();
        switch (key) {
            case FIRST_NAME:
                return criteriaBuilder.like(root.get("firstName"), "%" + value + "%");
            case LAST_NAME:
                return criteriaBuilder.like(root.get("lastName"), "%" + value + "%");
            case NATIONAL_ID:
                return criteriaBuilder.like(root.get("nationalId"), "%" + value + "%");
            case EMAIL:
                return criteriaBuilder.like(root.get("email"), "%" + value + "%");
            default:
                return null;
        }
    }

    enum SearchCriteriaKey {
        FIRST_NAME("firstName"),
        LAST_NAME("lastName"),
        NATIONAL_ID("nationalId"),
        EMAIL("email");
        private final String value;
        SearchCriteriaKey(String value) {
            this.value = value;
        }

        public static SearchCriteriaKey getKeyByValue(String keyValue) {
            return Arrays.stream(SearchCriteriaKey.values()).filter(searchCriteriaKey ->
                            searchCriteriaKey.value.equals(keyValue)).findFirst()
                    .orElseThrow(() -> new IllegalArgumentException("Invalid filter criteria"));
        }

        public String getValue() {
            return value;
        }
    }

}



